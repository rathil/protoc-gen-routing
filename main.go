package main

import (
	"github.com/gogo/protobuf/vanity/command"
)

func main() {
	req := command.Read()
	rout := &routing{}
	resp := command.GeneratePlugin(req, rout, "_routing.gen.go")
	command.Write(resp)
}

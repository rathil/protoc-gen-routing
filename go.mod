module gitlab.com/rathil/protoc-gen-routing

go 1.14

require (
	github.com/gofiber/fiber/v2 v2.1.1
	github.com/gogo/protobuf v1.3.2
)

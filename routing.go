package main

import (
	"github.com/gogo/protobuf/gogoproto"
	"github.com/gogo/protobuf/protoc-gen-gogo/generator"
	"regexp"
	"strings"
)

var (
	nameToUrlRegex = regexp.MustCompile(`([A-Z][a-z0-9]*)`)
)

type routing struct {
	*generator.Generator
	generator.PluginImports
	localName string
}

func (a *routing) Name() string {
	return "routing"
}

func (a *routing) Init(g *generator.Generator) {
	a.Generator = g
	a.PluginImports = generator.NewPluginImports(a.Generator)
}

func (a *routing) Generate(file *generator.FileDescriptor) {
	if !gogoproto.IsProto3(file.FileDescriptorProto) {
		return
	}
	needBuild := false
	for _, service := range file.Service {
		if len(service.Method) > 0 {
			needBuild = true
			break
		}
	}
	if !needBuild {
		return
	}
	fiberPkg := a.NewImport("github.com/gofiber/fiber/v2")
	fName := generator.CamelCase(file.GoPackageName())
	a.P(`// Registers all routes of `, fName)
	a.P(`func RoutesAll`, fName, `(router `, fiberPkg.Use(), `.Router, server *ServerAll`, fName, `) {`)
	a.In()
	for _, service := range file.Service {
		if len(service.Method) == 0 {
			continue
		}
		sName := generator.CamelCase(*service.Name)
		a.P(`Routes`, sName, `(router, server.`, sName, `)`)
	}
	a.Out()
	a.P(`}`)

	a.P(`// Register all servers of `, fName)
	a.P(`type ServerAll`, fName, ` struct {`)
	a.In()
	for _, service := range file.Service {
		if len(service.Method) == 0 {
			continue
		}
		sName := generator.CamelCase(*service.Name)
		a.P(sName, ` IServer`, sName)
	}
	a.Out()
	a.P(`}`)

	urlPrefix := strings.ReplaceAll(file.GetPackage(), ".", "/")
	for _, service := range file.Service {
		if len(service.Method) == 0 {
			continue
		}
		sName := generator.CamelCase(*service.Name)
		sUrl := a.nameToUrl(*service.Name)
		a.P(`// Register a routes of `, sName, ` service`)
		a.P(`func Routes`, sName, `(router `, fiberPkg.Use(), `.Router, server IServer`, sName, `) {`)
		a.In()
		for _, method := range service.Method {
			mName := generator.CamelCase(*method.Name)
			a.P(`router.Post(`)
			a.In()
			a.P(`"/`, urlPrefix, `/`, sUrl, `/`, a.nameToUrl(*method.Name), `/",`)
			a.P(`func(ctx *`, fiberPkg.Use(), `.Ctx) error {`)
			a.In()
			a.P(`ctx.Response().Header.SetContentType("application/protobuf")`)
			a.P(`var request `, a.TypeName(a.ObjectNamed(method.GetInputType())))
			a.P(`if err := proto.Unmarshal(nil, &request); err != nil {`)
			a.In()
			a.P(`return server.ErrorOnUnmarshal(ctx.Status(`, fiberPkg.Use(), `.StatusInternalServerError), err)`)
			a.Out()
			a.P(`}`)
			a.P(`response := &`, a.TypeName(a.ObjectNamed(method.GetOutputType())), `{}`)
			a.P(`status, err := server.`, mName, `(ctx, request, response)`)
			a.P(`if err != nil {`)
			a.In()
			a.P(`return err`)
			a.Out()
			a.P(`}`)
			a.P(`data, err := proto.Marshal(response)`)
			a.P(`if err != nil {`)
			a.In()
			a.P(`return server.ErrorOnMarshal(ctx.Status(`, fiberPkg.Use(), `.StatusInternalServerError), err)`)
			a.Out()
			a.P(`}`)
			a.P(`if status == 0 {`)
			a.In()
			a.P(`status = `, fiberPkg.Use(), `.StatusOK`)
			a.Out()
			a.P(`}`)
			a.P(`return ctx.Status(status).Send(data)`)
			a.Out()
			a.P(`},`)
			a.Out()
			a.P(`)`)
		}
		a.Out()
		a.P(`}`)

		a.P(`// Registers a routes of `, sName, ` service`)
		a.P(`type IServer`, sName, ` interface {`)
		a.In()
		a.P(`ErrorOnUnmarshal(ctx *`, fiberPkg.Use(), `.Ctx, err error) error`)
		a.P(`ErrorOnMarshal(ctx *`, fiberPkg.Use(), `.Ctx, err error) error`)
		for _, method := range service.Method {
			mName := generator.CamelCase(*method.Name)
			a.P(mName, `(ctx *`, fiberPkg.Use(), `.Ctx, request `, a.TypeName(a.ObjectNamed(method.GetInputType())), `, response *`, a.TypeName(a.ObjectNamed(method.GetOutputType())), `) (status_ int, err_ error)`)
		}
		a.Out()
		a.P(`}`)
	}
}

func (a *routing) nameToUrl(name string) string {
	var result []string
	items := nameToUrlRegex.FindAllString(name, -1)
	for _, item := range items {
		if len(item) > 1 {
			result = append(result, item)
		} else {
			if len(result) == 0 {
				result = append(result, "")
			}
			result[len(result)-1] = result[len(result)-1] + item
		}
	}
	return strings.ToLower(strings.Join(result, "-"))
}
